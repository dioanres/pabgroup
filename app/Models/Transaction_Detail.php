<?php

/**
 * Model genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transaction_Detail extends Model
{
	use SoftDeletes;

	protected $table = 'transaction_detail';

	protected $hidden = [];

	protected $fillable = ['id', 'transaction_id', 'product_id', 'price', 'qty', 'profit', 'customer_id', 'discount_amount', 'total_net_amount', 'notes', 'customer_credit_id', 'created_by', 'updated_by'];

	protected $guarded = [];

	protected $dates = ['deleted_at'];
}
