<?php
/**
 * Model genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transaction extends Model
{
    use SoftDeletes;
	
	protected $table = 'transaction';
	
	protected $hidden = [
        
    ];

    protected $fillable = ['id', 'date', 'notes', 'division_id', 'total_amount', 'total_discount', 'total_profit'];

	protected $guarded = [];

	protected $dates = ['deleted_at'];
}
