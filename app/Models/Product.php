<?php

/**
 * Model genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Product extends Model
{
	use SoftDeletes;

	protected $table = 'product';

	protected $hidden = [];

	protected $guarded = [];

	protected $dates = ['deleted_at'];

	public static function listProduk()
	{
		$role = Auth()->user()->role_users->role_id;
		if ($role == config('global.role.superadmi_pab')) {
			$divisi = [1, 2, 3, 4];
			$namaDiv = 'admin';
		} else if ($role == config('global.role.admin_infrastruktur')) {
			$divisi = [1];
			$namaDiv = 'infra';
		} else if ($role == config('global.role.admin_mimado')) {
			$divisi = [2];
			$namaDiv = 'mimado';
		} else {
			$divisi = [1, 2, 3, 4, 5];
			$namaDiv = 'admin';
		}
		$sql = "SELECT id, IFNULL(code, '') code, description, product_group_id, stock, price from product WHERE product_group_id in (SELECT id from product_group WHERE division_id in (?))";
		$listProduk = DB::select(DB::raw($sql), [implode(",",$divisi)]);

		$data = array('listProduk' => $listProduk, 'divisi' => $namaDiv, 'id_divisi' => $divisi);
		return $data;
	}
}
