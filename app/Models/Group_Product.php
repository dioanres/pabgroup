<?php

/**
 * Model genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Group_Product extends Model
{

	use SoftDeletes;

	protected $table = 'product_group';

	protected $hidden = [];
	protected $guarded = [];

	protected $dates = ['deleted_at'];

	public static function listGrupProduk()
	{
		$role = Auth()->user()->role_users->role_id;
		if ($role == config('global.role.superadmi_pab')) {
			$divisi = [1, 2, 3, 4];
			$namaDiv = 'admin';
		} else if ($role == config('global.role.admin_infrastruktur')) {
			$divisi = [1];
			$namaDiv = 'infra';
		} else if ($role == config('global.role.admin_mimado')) {
			$divisi = [2];
			$namaDiv = 'mimado';
		} else {
			$divisi = [1, 2, 3, 4, 5];
			$namaDiv = 'admin';
		}
		$listGrupProduk = Group_Product::select('id', 'description')->whereIn('division_id', $divisi)->get()->toArray();

		$data = array('listGrupProduk' => $listGrupProduk, 'divisi' => $namaDiv, 'id_divisi' => $divisi);
		return $data;
	}
}
