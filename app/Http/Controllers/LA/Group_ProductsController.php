<?php

/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use DB;
use Validator;
use Datatables;
use Collective\Html\FormFacade as Form;
use Dwij\Laraadmin\Models\Module;
use Dwij\Laraadmin\Models\ModuleFields;

use App\Models\Group_Product;

class Group_ProductsController extends Controller
{
	public $show_action = true;
	public $view_col = 'description';
	public $listing_cols = ['id', 'description', 'division_id'];
	public $divisi = '';

	public function __construct()
	{
		// Field Access of Listing Columns
		if (\Dwij\Laraadmin\Helpers\LAHelper::laravel_ver() == 5.3) {
			$this->middleware(function ($request, $next) {
				$this->listing_cols = ModuleFields::listingColumnAccessScan('Group_Products', $this->listing_cols);
				return $next($request);
			});
		} else {
			$this->listing_cols = ModuleFields::listingColumnAccessScan('Group_Products', $this->listing_cols);
		}
	}

	/**
	 * Display a listing of the Group_Products.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$module = Module::get('Group_Products');
		if (Module::hasAccess($module->id)) {
			return View('la.group_products.index', [
				'show_actions' => $this->show_action,
				'listing_cols' => $this->listing_cols,
				'module' => $module
			]);
		} else {
			return redirect(config('laraadmin.adminRoute') . "/");
		}
	}

	/**
	 * Show the form for creating a new group_product.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created group_product in database.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{

		$role = $this->role();
		if ($role == config('global.role.superadmin_pab')) {
			$divisi = $request->division_id;
		} else if ($role == config('global.role.admin_infrastruktur')) {
			$divisi = config('global.divisi.div_infrastruktur');
		} else if ($role == config('global.role.admin_mimado')) {
			$divisi = config('global.divisi.div_mimado');
		} else {
			$divisi = $request->division_id;
		}

		if (Module::hasAccess("Group_Products", "create")) {

			$rules = Module::validateRules("Group_Products", $request);

			$validator = Validator::make($request->all(), $rules);

			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();
			}
			$id = DB::table('product_group')->select('id')->orderBy('id', 'DESC')->first();
			$id = $id->id + 1;
			$insert_id = Group_Product::insert(['id' => $id, 'description' => $request->description, 'division_id' => $divisi]);

			return redirect()->route(config('laraadmin.adminRoute') . '.group_products.index');

		} else {
			return redirect(config('laraadmin.adminRoute') . "/");
		}
	}

	/**
	 * Display the specified group_product.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		if (Module::hasAccess("Group_Products", "view")) {

			$group_product = Group_Product::find($id);
			if (isset($group_product->id)) {
				$module = Module::get('Group_Products');
				$module->row = $group_product;

				return view('la.group_products.show', [
					'module' => $module,
					'view_col' => $this->view_col,
					'no_header' => true,
					'no_padding' => "no-padding"
				])->with('group_product', $group_product);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("group_product"),
				]);
			}
		} else {
			return redirect(config('laraadmin.adminRoute') . "/");
		}
	}

	/**
	 * Show the form for editing the specified group_product.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		if (Module::hasAccess("Group_Products", "edit")) {
			$group_product = Group_Product::find($id);
			if (isset($group_product->id)) {
				$module = Module::get('Group_Products');

				$module->row = $group_product;

				return view('la.group_products.edit', [
					'module' => $module,
					'view_col' => $this->view_col,
				])->with('group_product', $group_product);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("group_product"),
				]);
			}
		} else {
			return redirect(config('laraadmin.adminRoute') . "/");
		}
	}

	/**
	 * Update the specified group_product in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		$divisi = '';
		$role = $this->role();
		if ($role == config('global.role.superadmin_pab')) {
			$divisi = $request->division_id;
		} else if ($role == config('global.role.admin_infrastruktur')) {
			$divisi = config('global.divisi.div_infrastruktur');
		} else if ($role == config('global.role.admin_mimado')) {
			$divisi = config('global.divisi.div_mimado');
		} else {
			$divisi = $request->division_id;
		}

		if (Module::hasAccess("Group_Products", "edit")) {

			$rules = Module::validateRules("Group_Products", $request, true);

			$validator = Validator::make($request->all(), $rules);

			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();;
			}

			$update = Group_Product::find($id);
			$update->description = $request->description;
			$update->division_id = $divisi;
			$update->save();

			return redirect()->route(config('laraadmin.adminRoute') . '.group_products.index');

		} else {
			return redirect(config('laraadmin.adminRoute') . "/");
		}
	}

	/**
	 * Remove the specified group_product from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		if (Module::hasAccess("Group_Products", "delete")) {
			Group_Product::find($id)->delete();
			
			// Redirecting to index() method
			return redirect()->route(config('laraadmin.adminRoute') . '.group_products.index');
		} else {
			return redirect(config('laraadmin.adminRoute') . "/");
		}
	}

	/**
	 * Datatable Ajax fetch
	 *
	 * @return
	 */
	public function dtajax()
	{
		$role = $this->role();
		if ($role == config('global.role.superadmi_pab')) {
			$divisi = [1, 2, 3, 4];
		} else if ($role == config('global.role.admin_infrastruktur')) {
			$divisi = [1];
		} else if ($role == config('global.role.admin_mimado')) {
			$divisi = [2];
		} else {
			$divisi = [1, 2, 3, 4, 5];
		}

		$values = DB::table('product_group')->select($this->listing_cols)->whereIn('division_id', $divisi)->whereNull('deleted_at');
		$out = Datatables::of($values)->make();
		$data = $out->getData();

		$fields_popup = ModuleFields::getModuleFields('Group_Products');

		for ($i = 0; $i < count($data->data); $i++) {
			for ($j = 0; $j < count($this->listing_cols); $j++) {
				$col = $this->listing_cols[$j];
				if ($fields_popup[$col] != null && starts_with($fields_popup[$col]->popup_vals, "@")) {
					$data->data[$i][$j] = ModuleFields::getFieldValue($fields_popup[$col], $data->data[$i][$j]);
				}
				if ($col == $this->view_col) {
					$data->data[$i][$j] = '<a href="' . url(config('laraadmin.adminRoute') . '/group_products/' . $data->data[$i][0]) . '">' . $data->data[$i][$j] . '</a>';
				}
				// else if($col == "author") {
				//    $data->data[$i][$j];
				// }
			}

			if ($this->show_action) {
				$output = '';
				if (Module::hasAccess("Group_Products", "edit")) {
					$output .= '<a href="' . url(config('laraadmin.adminRoute') . '/group_products/' . $data->data[$i][0] . '/edit') . '" class="btn btn-warning btn-xs" style="display:inline;padding:2px 5px 3px 5px;"><i class="fa fa-edit"></i></a>';
				}

				if (Module::hasAccess("Group_Products", "delete")) {
					$output .= Form::open(['route' => [config('laraadmin.adminRoute') . '.group_products.destroy', $data->data[$i][0]], 'method' => 'delete', 'style' => 'display:inline']);
					$output .= ' <button class="btn btn-danger btn-xs" type="submit"><i class="fa fa-times"></i></button>';
					$output .= Form::close();
				}
				$data->data[$i][] = (string)$output;
			}
		}
		$out->setData($data);
		return $out;
	}

	public function role()
	{
		return Auth()->user()->role_users->role_id;
	}
}
