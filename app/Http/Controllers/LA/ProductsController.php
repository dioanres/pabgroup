<?php

/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use DB;
use Validator;
use Datatables;
use Collective\Html\FormFacade as Form;
use Dwij\Laraadmin\Models\Module;
use Dwij\Laraadmin\Models\ModuleFields;

use App\Models\Product;
use App\Models\Group_Product;

class ProductsController extends Controller
{
	public $show_action = true;
	public $view_col = 'stock';
	public $listing_cols = ['id', 'code', 'description', 'product_group_id', 'stock', 'price'];

	public function __construct()
	{
		// Field Access of Listing Columns
		if (\Dwij\Laraadmin\Helpers\LAHelper::laravel_ver() == 5.3) {
			$this->middleware(function ($request, $next) {
				$this->listing_cols = ModuleFields::listingColumnAccessScan('Products', $this->listing_cols);
				return $next($request);
			});
		} else {
			$this->listing_cols = ModuleFields::listingColumnAccessScan('Products', $this->listing_cols);
		}
	}

	/**
	 * Display a listing of the Products.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{

		$module = Module::get('Products');
		$dataByRole = $this->listGrupProduk();
		if (Module::hasAccess($module->id)) {
			return View('la.products.index', [
				'show_actions' => $this->show_action,
				'listing_cols' => $this->listing_cols,
				'module' => $module,
				'list_group_product' => $dataByRole['listGrupProduk'],
				'divisi' => $dataByRole['divisi']
			]);
		} else {
			return redirect(config('laraadmin.adminRoute') . "/");
		}
	}

	/**
	 * Show the form for creating a new product.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created product in database.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$divisi = '';

		$role = $this->role();

		if ($role == config('global.role.superadmin_pab')) {

			$divisi = $request->division_id;

		} else if ($role == config('global.role.admin_infrastruktur')) {

			$divisi = config('global.divisi.div_infrastruktur');

		} else if ($role == config('global.role.admin_mimado')) {

			$divisi = config('global.divisi.div_mimado');

		} else {

			$divisi = $request->division_id;
		}
		if (Module::hasAccess("Products", "create")) {

			$rules = Module::validateRules("Products", $request);

			$validator = Validator::make($request->all(), $rules);

			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();
			}

			$insert_id = Module::insert("Products", $request);

			return redirect()->route(config('laraadmin.adminRoute') . '.products.index');

		} else {
			return redirect(config('laraadmin.adminRoute') . "/");
		}
	}

	/**
	 * Display the specified product.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		if (Module::hasAccess("Products", "view")) {

			$product = Product::find($id);
			if (isset($product->id)) {
				$module = Module::get('Products');
				$module->row = $product;

				return view('la.products.show', [
					'module' => $module,
					'view_col' => $this->view_col,
					'no_header' => true,
					'no_padding' => "no-padding"
				])->with('product', $product);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("product"),
				]);
			}
		} else {
			return redirect(config('laraadmin.adminRoute') . "/");
		}
	}

	/**
	 * Show the form for editing the specified product.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		$dataByRole = $this->listGrupProduk();
		if (Module::hasAccess("Products", "edit")) {
			$product = Product::find($id);
			//dd($product);
			if (isset($product->id)) {
				$module = Module::get('Products');

				$module->row = $product;

				return view('la.products.edit', [
					'module' => $module,
					'view_col' => $this->view_col,
					'list_group_product' => $dataByRole['listGrupProduk'],
					'divisi' => $dataByRole['divisi']

				])->with('product', $product);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("product"),
				]);
			}
		} else {
			return redirect(config('laraadmin.adminRoute') . "/");
		}
	}

	/**
	 * Update the specified product in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		if (Module::hasAccess("Products", "edit")) {

			$rules = Module::validateRules("Products", $request, true);

			$validator = Validator::make($request->all(), $rules);

			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();;
			}

			$insert_id = Module::updateRow("Products", $request, $id);

			return redirect()->route(config('laraadmin.adminRoute') . '.products.index');

		} else {
			return redirect(config('laraadmin.adminRoute') . "/");
		}
	}

	/**
	 * Remove the specified product from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		if (Module::hasAccess("Products", "delete")) {
			Product::find($id)->delete();
			
			// Redirecting to index() method
			return redirect()->route(config('laraadmin.adminRoute') . '.products.index');
		} else {
			return redirect(config('laraadmin.adminRoute') . "/");
		}
	}

	/**
	 * Datatable Ajax fetch
	 *
	 * @return
	 */
	public function dtajax()
	{
		//$values = DB::table('product')->select($this->listing_cols)->whereNull('deleted_at');

		$divisi = $this->listGrupProduk();
		//dd($produkGroupId, $divisi['id_divisi']);
		$produkGroupId = Group_Product::select('id')->whereIn('division_id', $divisi['id_divisi'])->get()->toArray();
		$values = DB::table('product')->select($this->listing_cols)->whereNull('deleted_at')->whereIn('product_group_id', $produkGroupId);
		$out = Datatables::of($values)->make();
		$data = $out->getData();

		$fields_popup = ModuleFields::getModuleFields('Products');

		for ($i = 0; $i < count($data->data); $i++) {
			for ($j = 0; $j < count($this->listing_cols); $j++) {
				$col = $this->listing_cols[$j];
				if ($fields_popup[$col] != null && starts_with($fields_popup[$col]->popup_vals, "@")) {
					$data->data[$i][$j] = ModuleFields::getFieldValue($fields_popup[$col], $data->data[$i][$j]);
				}
				if ($col == $this->view_col) {
					$data->data[$i][$j] = '<a href="' . url(config('laraadmin.adminRoute') . '/products/' . $data->data[$i][0]) . '">' . $data->data[$i][$j] . '</a>';
				}
				// else if($col == "author") {
				//    $data->data[$i][$j];
				// }
			}

			if ($this->show_action) {
				$output = '';
				if (Module::hasAccess("Products", "edit")) {
					$output .= '<a href="' . url(config('laraadmin.adminRoute') . '/products/' . $data->data[$i][0] . '/edit') . '" class="btn btn-warning btn-xs" style="display:inline;padding:2px 5px 3px 5px;"><i class="fa fa-edit"></i></a>';
				}

				if (Module::hasAccess("Products", "delete")) {
					$output .= Form::open(['route' => [config('laraadmin.adminRoute') . '.products.destroy', $data->data[$i][0]], 'method' => 'delete', 'style' => 'display:inline']);
					$output .= ' <button class="btn btn-danger btn-xs" type="submit"><i class="fa fa-times"></i></button>';
					$output .= Form::close();
				}
				$data->data[$i][] = (string)$output;
			}
		}
		$out->setData($data);
		return $out;
	}

	public function role()
	{
		return Auth()->user()->role_users->role_id;
	}

	public function listGrupProduk()
	{
		$ret = Group_Product::listGrupProduk();
		$role = $this->role();
		if ($role == config('global.role.superadmi_pab')) {
			$divisi = [1, 2, 3, 4];
			$namaDiv = 'admin';
		} else if ($role == config('global.role.admin_infrastruktur')) {
			$divisi = [1];
			$namaDiv = 'infra';
		} else if ($role == config('global.role.admin_mimado')) {
			$divisi = [2];
			$namaDiv = 'mimado';
		} else {
			$divisi = [1, 2, 3, 4, 5];
			$namaDiv = 'admin';
		}
		$listGrupProduk = Group_Product::select('id', 'description')->where('division_id', $divisi)->get()->toArray();

		$data = array('listGrupProduk' => $listGrupProduk, 'divisi' => $namaDiv, 'id_divisi' => $divisi);
		return $data;
	}
}
