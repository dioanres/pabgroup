<?php

/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function index()
    {
        $roleCount = \App\Role::count();
        if ($roleCount != 0) {
            if ($roleCount != 0) {
                //dd(Auth()->user());
                if (Auth()->user()) {
                    //dd(Auth()->user()->role_users->role_id);
                    //dd(Auth()->user()->role_users->role_id);
                    //return view('la.dashboard');
                    return view('home1');
                } else {
                    //return view('home1');
                    return view('auth.login');
                }
                //return view('home');
            }
        } else {
            return view('errors.error', [
                'title' => 'Migration not completed',
                'message' => 'Please run command <code>php artisan db:seed</code> to generate required table data.',
            ]);
        }
    }
}