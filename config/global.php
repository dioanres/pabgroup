<?php

return [

    "divisi" => [
        'div_infrastruktur' => 1,
        'div_mimado' => 2,
        'div_king' => '',
        'div_pasti' => ''
    ],

    'role' => [
        'superadmin_panel' => 1,
        'superadmin_pab' => 5,
        'admin_infrastruktur' => 3,
        'admin_mimado' => 4,
        'admin_king' => '',
        'admin_pasti' => ''
    ]

];