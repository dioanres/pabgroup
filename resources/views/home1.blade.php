

<style>
	body {
		background-color: #0e0d17;
		display: block;
		margin: 0px;
	}

	.bg {
		
		background: url(bg/6.jpg) no-repeat center/cover;
		/* opacity: 0.23; */
		height: 100%;
		width: 100%;
		position: absolute;
	}

	.icon {
		width: 200px;
		background-color: white;
		border-radius: 50px;
		box-shadow: 0px -1px 34px -6px grey 
	}
	.flex {
		margin-top: 15%;
		/* margin-left: 14%; */
		margin-right: 0%;
	}
	ul {
		list-style-type: none;
		display: inline-block;
		margin: 0;
		padding: 0;
		overflow: hidden;
		/* background-color: #333333; */
		}

	ul li {
		margin: 10px;
	}

	li {
		float: left;
	}

	li a {
		display: block;
		color: #423f3f;
		text-align: center;
		padding: 4px;
		text-decoration: none;
	}

	li a:hover {
		background-color: #808080;
		border-radius: 50px; 
	}

	.limiter {
		width: 100%;
		margin: 0 auto;
	}
</style>
<body>


<div class="limiter">


<div class="bg container text-center">
	<!-- <div class="wrapper">
		
	</div> -->
	<div class="row flex">
		<div class="col-md-12">
			
			<center>
			<ul>
				@if (Auth()->user()->role_users->role_id == 1 || Auth()->user()->role_users->role_id == 5)
				<li>
					<div class="col-md-3">
						<div class="icon">
							<a href="{{url('admin/dashboard')}}">
							<img src="{{ asset('/icon/admin.png') }}" alt="">
							</a>
						</div>
					</div>
				</li>
				@endif

				@if (Auth()->user()->role_users->role_id == 3 || Auth()->user()->role_users->role_id == 1 || Auth()->user()->role_users->role_id == 5)
				<li>
					<div class="col-md-3">
						<div class="icon">
							<a href="{{url('admin/dashboard')}}">
							<img src="{{ asset('/icon/pab1.png') }}" alt="">
							</a>
						</div>
					</div>
				</li>
				@endif

				@if (Auth()->user()->role_users->role_id == 4 || Auth()->user()->role_users->role_id == 1 || Auth()->user()->role_users->role_id == 5)
				<li>
					<div class="col-md-3">
						<div class="icon">
							<a href="{{url('admin/dashboard')}}">
							<img src="{{ asset('/icon/mimado.png') }}" alt="">
							</a>
						</div>
					</div>
				</li>
				@endif

				@if (Auth()->user()->role_users->role_id == 6 || Auth()->user()->role_users->role_id == 1 || Auth()->user()->role_users->role_id == 5)
				<li>
					<div class="col-md-3">
						<div class="icon">
							<a href="{{url('admin/dashboard')}}">
							<img src="{{ asset('/icon/king.png') }}" alt="">
							</a>
						</div>
					</div>
				</li>
				@endif

				@if (Auth()->user()->role_users->role_id == 7 || Auth()->user()->role_users->role_id == 1 || Auth()->user()->role_users->role_id == 5)
				<li>
					<div class="col-md-3">
						<div class="icon">
							<a href="{{url('admin/dashboard')}}">
							<img src="{{ asset('/icon/pasti.png') }}" alt="">
							</a>
						</div>
					</div>
				</li>
				@endif
			</ul>
			</center>
			
		</div>
	</div>
</div>
</div>

</body>