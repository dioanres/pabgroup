@extends("la.layouts.app")

@section("contentheader_title", "Tambah Transaksi")
@section("contentheader_description", "Daftar Transaksi")
@section("section", "Transaksi")
@section("sub_section", "Daftar")
@section("htmlheader_title", "Daftar Transaksi")

@section("headerElems")
	<a class="btn btn-success btn-sm pull-right" id="add-transaction">Tambah Transaksi</a>
@endsection

@section("main-content")

<!-- @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif -->

<div class="box box-success">
	<!--<div class="box-header"></div>-->
	<div class="box-body">
		<table id="example1" class="table table-bordered">
			<thead>
				<tr class="success">
					@foreach( $listing_cols as $col => $index )
					<th>{{$listing_cols_labels[$col]}}</th>
					@endforeach
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
				
			</tbody>
		</table>
	</div>
</div>

@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/plugins/datatables/datatables.min.css') }}"/>
@endpush

@push('scripts')
<script src="{{ asset('la-assets/plugins/datatables/datatables.min.js') }}"></script>
<script>
	$(function () {
		$('#add-transaction').click(function(){
			location.href = "{{ url(config('laraadmin.adminRoute')) }}"+'/add_transaction/'+last_url;
		});

		$("#example1").DataTable({
			processing: true,
	        serverSide: true,
	        ajax: "{{ url(config('laraadmin.adminRoute')) }}"+'/transaction_dt_ajax/'+last_url,
			language: {
				lengthMenu: "_MENU_",
				search: "_INPUT_",
				searchPlaceholder: "Search"
			},
			columnDefs: [ { orderable: false, targets: [-1] }],
		});
	});
</script>
@endpush
