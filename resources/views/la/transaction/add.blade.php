@extends("la.layouts.app")

@section("contentheader_title", "Tambah Transaksi")
@section("contentheader_description", "Daftar Transaksi")
@section("section", "Transaksi")
@section("sub_section", "Tambah Transaksi")
@section("htmlheader_title", "Tambah Transaksi")

@section("main-content")

<div class="box box-success">
	<!--<div class="box-header"></div>-->
	<div class="box-body">
		<form id="form-tambah-transaksi" method="POST" enctype="multipart/form-data" action="{{ url('admin/create_transaction') }}">
            {{ csrf_field() }}
            <fieldset>
            	<div class="row" style="margin-top: 10px;">

            		<input type="hidden" name="division_id" id="division_id">

	            	<div class="col-sm-6">
	            		<div class="row form-group m-b-10">
		                  	<label class="col-md-3 col-form-label" id="no-">Tanggal Transaksi</label>
		                  	<div class="col-md-9">
			                    <div class="input-group datetimepicker" id="tanggal-transaksi">
			                        <input type="text" name="date" class="form-control" />
			                        <div class="input-group-addon">
			                            <i class="fa fa-calendar"></i>
			                        </div>
			                    </div>
		                  	</div>
		                </div>

		                <div class="row form-group m-b-10">
		                  <label class="col-md-3 col-form-label" id="no-">Catatan</label>
		                  	<div class="col-md-9">
			                    <textarea class="textarea" name="notes" id="notes" style="width: 100%; height: 125px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
		                  	</div>
		                </div>
	            	</div>
            	
            		<div class="col-sm-6">
            			<div class="row form-group m-b-10">
                          	<label class="col-md-3 col-form-label" id="no-">Diskon (%)</label>
                          	<div class="col-md-9">
                            	<input type="text" name="discount_percentage" id="discount_percentage" class="form-control" />
                          	</div>
                        </div>

                        <div class="row form-group m-b-10">
                          	<label class="col-md-3 col-form-label" id="no-">Diskon (IDR)</label>
                          	<div class="col-md-9">
                            	<input type="text" name="total_discount" id="total_discount" class="form-control" />
                          	</div>
                        </div>

            			<div class="row form-group m-b-10">
                          	<label class="col-md-3 col-form-label" id="no-">Total Setelah Diskon</label>
                          	<div class="col-md-9">
                            	<input type="text" name="total_amount" id="total_amount" class="form-control" readonly="readonly" />
                          	</div>
                        </div>

                        <div class="row form-group m-b-10">
                          	<label class="col-md-3 col-form-label" id="no-">Profit Setelah Diskon</label>
                          	<div class="col-md-9">
                            	<input type="text" name="total_profit" id="total_profit" class="form-control" readonly="readonly" />
                          	</div>
                        </div>
	            	</div>

            	</div>
            </fieldset>

	        <div class="row">
	        	<div class="col-sm-12">
		        	<span class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target="#AddModal">Tambah Detail Transaksi</span>
		        </div>
	        </div>

	        <div class="row" style="margin-top: 10px;">
	        	<div class="col-sm-12">
		        	<table id="detail-transaksi" class="table table-bordered table-striped">
		        		<thead>
		        			<tr class="success">
			        			<th>Produk</th>
			        			<th>QTY</th>
			        			<th>Pelanggan</th>
			        			<th>Profit per Qty</th>
			        			<th>Diskon</th>
			        			<th>Harga Bersih</th>
			        			<th>Referensi Transaksi</th>
			        			<th>Catatan</th>
			        			<th>Opsi</th>
			        		</tr>
		        		</thead>
		        		<tbody>
		        			
		        		</tbody>
		        	</table>
		        </div>
	        </div>

	        <div class="row" style="margin-top: 10px;">
	        	<div class="col-sm-12">
	        		<input class="btn btn-success btn-sm pull-right" type="submit" name="SIMPAN">
		        </div>
	        </div>
	        
        </form>
	</div>
</div>

<div class="modal fade" id="AddModal" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Tambah Detail Transaksi</h4>
			</div>

			<div class="modal-body">
				<div class="box-body">
					<form id="transaction-detail" action="#">
						<div class="form-group" >
							<label for="product_group_id">Grup Produk</label>
							<select class="form-control selectto" name="product_group_id" id="product_group_id" rel="select2" required="1" aria-required="true">
									<option value="">Pilih Grup Produk</option>
								@foreach ($list_group_product as $value)
									<option value="{{ $value['id'] }}">{{ $value['description'] }}</option>
								@endforeach
							</select>
						</div>

						<div class="form-group" >
							<label for="product_id">Produk</label>
							<select class="form-control selectto" name="product_id" id="product_id" rel="select2" required="1" aria-required="true">
									<option value="">Pilih Produk</option>
							</select>
						</div>

						<div class="form-group">
							<label for="price">Harga :</label>
							<input class="form-control" name="price" id="price" type="number" readonly="readonly">
						</div>

						<div class="form-group">
							<label for="profit">Profit per Qty :</label>
							<input class="form-control" name="profit" id="profit" type="number">
						</div>

						<div class="form-group">
							<label for="qty">Qty :</label>
							<input class="form-control" name="qty" id="qty" type="number" value="1">
						</div>

						<div class="form-group">
							<label for="qty">Diskon (IDR) :</label>
							<input class="form-control" name="discount_amount" id="discount_amount" type="number" value="0">
						</div>

						<div class="form-group">
							<label for="total_net_amount">Harga Bersih :</label>
							<input class="form-control" name="total_net_amount" id="total_net_amount" type="number" readonly="readonly">
						</div>

						<div class="form-group">
							<label for="profit">Catatan :</label>
							<textarea class="textarea" name="notes" id="notes-detail" style="width: 100%; height: 125px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-success" data-dismiss="modal" id="tambah-detail">Tambah</button>
				</div>
			</form>
		</div>
	</div>
</div>

@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/plugins/datatables/datatables.min.css') }}"/>
<link rel="stylesheet" href="{{ asset('la-assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">
@endpush

@push('scripts')
<script src="{{ asset('la-assets/plugins/datatables/datatables.min.js') }}"></script>
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<script src="{{ asset('la-assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
<script>
	var table_transaksi = $('#detail-transaksi').DataTable({
		"paging": false
	});

	$('#division_id').val(last_url);

	function refresh_produk(group_id){
		var list_product = {!! json_encode($list_product) !!};
		var product = $.grep(list_product, function(v) {
		    return v.product_group_id == group_id;
		});

		$("#product_id").select2('destroy').empty();

		var product_option = '<option value="" selected>Pilih Produk</option>';

		$.each(product, function(){
			product_option += '<option value="'+this['id']+'" price="'+this.price+'">' +this.code+ " " +this['description']+'</option>';
		});

		$('#product_id').append(product_option);

		$("#product_id").select2();
	}

	function remove_line(element){
		table_transaksi.row( $(element).parents('tr') ).remove().draw();
	}

	$(document).ready(function(){
		$('#product_group_id').change(function(){
		    refresh_produk($(this).val());
		});

		$('#product_id').change(function(){
			$('#price').val($(this).find('option:selected').attr('price'));
		});

		$('#tambah-detail').click(function(){
			var data = $("#transaction-detail").serializeArray();
			console.log(data);

			var data_post = {
				product_id : $('#product_id').val(),
				price : $('#price').val(),
				qty : $('#qty').val(),
				profit : $('#profit').val(),
				customer_id : '1',
				discount_amount : $('#discount_amount').val(),
				total_net_amount : '1',
				notes : '1',
				customer_credit_id : '1'
			}

			console.log(JSON.stringify(data_post));

			table_transaksi.row.add([
				$('#product_id').find('option:selected').text(),
				data[4].value,
				'1',
				data[3].value,
				data[5].value,
				data[6].value,
				'1',
				data[7].value,
				`<button class="btn btn-danger btn-xs" onclick="remove_line(this)" type="submit"><i class="fa fa-times"></i></button>
				<input type="hidden" name="detail[]" value='`+JSON.stringify(data_post)+`' >
				`
			]).draw().node();

			$("#transaction-detail")[0].reset();
			$('.selectto').select2("val", "");
		});
	});
	$(function () {
		
	});
</script>
@endpush
