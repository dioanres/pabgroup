@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/products') }}">Product</a> :
@endsection
@section("contentheader_description", $product->$view_col)
@section("section", "Products")
@section("section_url", url(config('laraadmin.adminRoute') . '/products'))
@section("sub_section", "Edit")

@section("htmlheader_title", "Products Edit : ".$product->$view_col)

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{!! Form::model($product, ['route' => [config('laraadmin.adminRoute') . '.products.update', $product->id ], 'method'=>'PUT', 'id' => 'product-edit-form']) !!}
					{{-- @la_form($module) --}}
					@if ($divisi == 'king')
					<div class="form-group">
						<label for="description">Kode Barang :</label>
						<input class="form-control" placeholder="Enter Kode Barang" data-rule-maxlength="256"  name="code" type="text" value="{{$product->code}}" aria-required="true">
					</div>
					@endif
					<div class="form-group" >
						<label for="description">Deskripsi Barang* :</label>
						<input class="form-control" placeholder="Enter Deskripsi Barang" data-rule-maxlength="256" required="1" name="description" type="text" value="{{$product->description}}" aria-required="true">
					</div>
					<div class="form-group" >
						<label for="product_group_id">Grup Produk</label>
						<select class="form-control" name="product_group_id" rel="select2" required="1" aria-required="true">
								<option value="">Pilih Grup Produk</option>
							@foreach ($list_group_product as $value)
								<option value="{{ $value['id'] }}" @if ($product->product_group_id == $value['id']) selected @endif >{{ $value['description'] }}</option>
							@endforeach
						</select>
					</div>
					@if ($divisi == 'mimado')
					<div class="form-group">
						<label for="stock">Stok :</label>
						<input class="form-control" placeholder="Enter Stock" data-rule-maxlength="256" name="stock" type="number" value="{{$product->stock}}" aria-required="true">
					</div>
					@endif
					<div class="form-group">
						<label for="price">Harga :</label>
						<input class="form-control valid" placeholder="Enter Harga" name="price" type="number" value="{{$product->price}}" aria-invalid="false">
					</div>
					
                    <br>
					<div class="form-group">
						{!! Form::submit( 'Update', ['class'=>'btn btn-success']) !!} <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/products') }}">Cancel</a></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#product-edit-form").validate({
		
	});
});
</script>
@endpush
