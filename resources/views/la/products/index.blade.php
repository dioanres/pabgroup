@extends("la.layouts.app")

@section("contentheader_title", "Products")
@section("contentheader_description", "Products listing")
@section("section", "Products")
@section("sub_section", "Listing")
@section("htmlheader_title", "Products Listing")

@section("headerElems")
@la_access("Products", "create")
	<button class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target="#AddModal">Add Product</button>
@endla_access
@endsection

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box box-success">
	<!--<div class="box-header"></div>-->
	<div class="box-body">
		<table id="example1" class="table table-bordered">
		<thead>
		<tr class="success">
			@foreach( $listing_cols as $col )
			<th>{{ $module->fields[$col]['label'] or ucfirst($col) }}</th>
			@endforeach
			@if($show_actions)
			<th>Actions</th>
			@endif
		</tr>
		</thead>
		<tbody>
			
		</tbody>
		</table>
	</div>
</div>

@la_access("Products", "create")
<div class="modal fade" id="AddModal" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Add Product</h4>
			</div>

			{!! Form::open(['action' => 'LA\ProductsController@store', 'id' => 'product-add-form']) !!}
			<div class="modal-body">
				<div class="box-body">
					@if ($divisi == 'king')
					<div class="form-group">
						<label for="description">Kode Barang :</label>
						<input class="form-control" placeholder="Enter Kode Barang" data-rule-maxlength="256"  name="code" type="text" value="" aria-required="true">
					</div>
					@endif
					<div class="form-group" >
						<label for="description">Deskripsi Barang* :</label>
						<input class="form-control" placeholder="Enter Deskripsi Barang" data-rule-maxlength="256" required="1" name="description" type="text" value="" aria-required="true">
					</div>
					<div class="form-group" >
						<label for="product_group_id">Grup Produk</label>
						<select class="form-control" name="product_group_id" rel="select2" required="1" aria-required="true">
								<option value="">Pilih Grup Produk</option>
							@foreach ($list_group_product as $value)
								<option value="{{ $value['id'] }}">{{ $value['description'] }}</option>
							@endforeach
						</select>
					</div>
					@if ($divisi == 'mimado')
					<div class="form-group">
						<label for="stock">Stok :</label>
						<input class="form-control" placeholder="Enter Stock" data-rule-maxlength="256" name="stock" type="number" value="" aria-required="true">
					</div>
					@endif
					<div class="form-group">
						<label for="price">Harga :</label>
						<input class="form-control valid" placeholder="Enter Harga" name="price" type="number" value="0" aria-invalid="false">
					</div>

                    {{-- @la_form($module) --}}
					
					
					{{--
					@la_input($module, 'code')
					@la_input($module, 'description')
					@la_input($module, 'product_group_id')
					@la_input($module, 'stock')
					@la_input($module, 'price')
					--}}
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				{!! Form::submit( 'Submit', ['class'=>'btn btn-success']) !!}
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
@endla_access

@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/plugins/datatables/datatables.min.css') }}"/>
@endpush

@push('scripts')
<script src="{{ asset('la-assets/plugins/datatables/datatables.min.js') }}"></script>
<script>
$(function () {
	$("#example1").DataTable({
		processing: true,
        serverSide: true,
        ajax: "{{ url(config('laraadmin.adminRoute') . '/product_dt_ajax') }}",
		language: {
			lengthMenu: "_MENU_",
			search: "_INPUT_",
			searchPlaceholder: "Search"
		},
		@if($show_actions)
		columnDefs: [ { orderable: false, targets: [-1] }],
		@endif
	});
	$("#product-add-form").validate({
		
	});
});
</script>
@endpush
