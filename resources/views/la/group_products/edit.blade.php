@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/group_products') }}">Group Product</a> :
@endsection
@section("contentheader_description", $group_product->$view_col)
@section("section", "Group Products")
@section("section_url", url(config('laraadmin.adminRoute') . '/group_products'))
@section("sub_section", "Edit")

@section("htmlheader_title", "Group Products Edit : ".$group_product->$view_col)

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{!! Form::model($group_product, ['route' => [config('laraadmin.adminRoute') . '.group_products.update', $group_product->id ], 'method'=>'PUT', 'id' => 'group_product-edit-form']) !!}
					@la_form($module)
					
					{{--
					@la_input($module, 'description')
					@la_input($module, 'division_id')
					--}}
                    <br>
					<div class="form-group">
						{!! Form::submit( 'Update', ['class'=>'btn btn-success']) !!} <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/group_products') }}">Cancel</a></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#group_product-edit-form").validate({
		
	});
});
</script>
@endpush
