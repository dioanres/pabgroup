@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/infrastrukturs') }}">Infrastruktur</a> :
@endsection
@section("contentheader_description", $infrastruktur->$view_col)
@section("section", "Infrastrukturs")
@section("section_url", url(config('laraadmin.adminRoute') . '/infrastrukturs'))
@section("sub_section", "Edit")

@section("htmlheader_title", "Infrastrukturs Edit : ".$infrastruktur->$view_col)

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{!! Form::model($infrastruktur, ['route' => [config('laraadmin.adminRoute') . '.infrastrukturs.update', $infrastruktur->id ], 'method'=>'PUT', 'id' => 'infrastruktur-edit-form']) !!}
					@la_form($module)
					
					{{--
					@la_input($module, 'id')
					@la_input($module, 'type')
					@la_input($module, 'pic')
					@la_input($module, 'jumlah')
					--}}
                    <br>
					<div class="form-group">
						{!! Form::submit( 'Update', ['class'=>'btn btn-success']) !!} <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/infrastrukturs') }}">Cancel</a></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#infrastruktur-edit-form").validate({
		
	});
});
</script>
@endpush
